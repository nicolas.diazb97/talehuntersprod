using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CharacterModel : MonoBehaviour
{
    public List<MeshRenderer> meshesOff;
    // Start is called before the first frame update
    void Start()
    {
        meshesOff = GetComponentsInChildren<MeshRenderer>().ToList();
        meshesOff.ForEach(v => v.enabled = false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
