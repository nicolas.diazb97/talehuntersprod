using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementManager : MonoBehaviour
{
    public Transform player;
    public Transform playerMesh;
    public Transform nextPos;
    public Transform[] positions;
    public int currLevel;
    public DiceManager diceManager;
    // Start is called before the first frame update
    void Start()
    {
        //NextModule();
        player.transform.LookAt(positions[currLevel + 1]);
    }

    // Update is called once per frame
    void Update()
    {
    }
    public void JumpAnimation()
    {
        player.GetComponentInChildren<Animator>().SetTrigger("Jump");
    }
    public void StartTranslate()
    {
        //esta linea setea el movimiento
        StartCoroutine(SetCharacterPosition(player, 0.7f));
        Debug.Log("se termino la animacion");
        //StartCoroutine(LerpPosition(player, nextPos.position, 0.7f));
    }
    IEnumerator SetCharacterPosition(Transform objToMove, float duration)
    {
        if (diceManager.currDiceValue > 0)
        {
            Vector3 targetPosition;
            targetPosition = positions[currLevel + 1].position;

                float time = 0;
            Vector3 startPosition = objToMove.transform.position;

            while (time < duration)
            {
                objToMove.transform.position = Vector3.Lerp(startPosition, targetPosition, time / duration);
                time += Time.deltaTime;
                yield return null;
            }
            currLevel = currLevel + 1;
            Debug.Log("se termino la animacion lerp");
            objToMove.transform.position = targetPosition;
            diceManager.currDiceValue = diceManager.currDiceValue - 1;
            objToMove.transform.GetComponentInChildren<Character>().transform.position = objToMove.transform.GetComponentInChildren<Character>().Target.position ;
            if (diceManager.currDiceValue != 0)
            {
                JumpAnimation();
            }
            if(positions[currLevel].GetComponent<TurningPoint>())
            {
                player.transform.LookAt(positions[currLevel + 3]);

            }
        }
    }
}
